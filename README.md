# PHP compatibility and syntax check
Script to verify if your PHP code has the right syntax and if it is compatible with an specific version.

### Basically it runs *PHP lint* and *PHP Code Sniffer*

### Docker required

## How to execute:
1. Copy the file **php-check.sh** to the root of your project
2. Run `chmod +x php-check.sh`
3. Run `./php-check.sh`

At the end of the execution, a folder called **php_syntax_and_compatibility_reports** will be created with the reports in it 

#### It is not compatible with windows

Feel free to change whatever you want
